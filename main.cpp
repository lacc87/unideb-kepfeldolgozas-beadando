#include "lotto_processor.cpp"

void showHelp() {
    cout << "\n" << "-----------------" << "\n";
    cout << "Help" << "\n";
    cout << "main show xxx.jpg - Show the founded numbers" << "\n";
    cout << "main run.sh pattern - Show the basic Lottery ticket" << "\n";
    cout << "main run.sh process xxx.jpg - Validate Lottery ticket" << "\n";
    cout << "-----------------" << "\n\n";
}
int main(int argc, char *argv[]) {

    if(argc == 1) {
        showHelp();
    } else {
        LottoProcessor process;
        if (strcmp(argv[1], "show") == 0) { 
            process.showFoundNumbers(argv[2]);

            int c = waitKey();

        } else if(strcmp(argv[1], "process") == 0) { 
            process.process(argv[2]);
        } else if(strcmp(argv[1], "pattern") == 0) { 
            process.showPattern();

            int c = waitKey();

        } else {
            showHelp();
        }
    }

	return 0;
}