#include "opencv2/core.hpp"
#include "opencv2/imgproc.hpp"
#include "opencv2/imgcodecs.hpp"
#include "opencv2/highgui.hpp"
#include <iostream>
using namespace cv;
using namespace std;

class LottoProcessor {
    public:
        void showPattern();
        void showFoundNumbers(char*);
        void process(char*);
    private:
        void drawSquares( Mat&, const vector<vector<Point> >& );
        void validateField( Mat&, const vector<vector<Point> >&, int );
        vector<vector<Point>> getPattern(int);
};
void LottoProcessor::showPattern()
{
    Mat image = imread("minta.jpg", IMREAD_GRAYSCALE);
    Mat image_fixed;
    threshold(image, image_fixed, 200, 255, THRESH_TOZERO);
    imshow("Numbers", image_fixed);
}
void LottoProcessor::showFoundNumbers(char* filename)
{
    Mat image = imread(filename, IMREAD_GRAYSCALE);
    Mat image_fixed;
    threshold(image, image_fixed, 200, 255, THRESH_TOZERO);
    for(int i = 0; i < 8; i++) {
        drawSquares(image_fixed, this->getPattern(i));
    }

}
void LottoProcessor::process(char* filename)
{
    Mat image = imread(filename, IMREAD_GRAYSCALE);
    Mat image_fixed;
    threshold(image, image_fixed, 200, 255, THRESH_TOZERO);
    for(int i = 0; i < 8; i++) {
        this->validateField(image_fixed, this->getPattern(i), i+1);
    }
}
void LottoProcessor::validateField( Mat& image, const vector<vector<Point> >& squares, int field )
{
    cout << "\n" << field << ". field numbers: ";
    int found_numbers = 0;
    for( size_t i = 0; i < squares.size(); i++ )
    {
        const Point* p = &squares[i][0];
        int n = (int)squares[i].size();
        int avg = 0;
        int sum = 0;

        for(int x = squares[i][0].x; x < squares[i][2].x; x++) {
            for(int y = squares[i][2].y; y < squares[i][0].y; y++) {
                Scalar intensity = image.at<uchar>(Point(x, y));
                avg += intensity.val[0];
                sum++;
            }
        }
        avg = round(avg / sum);
        if(avg < 140) {
            cout << 45 - i << " ";
            found_numbers++;
        }
    }
    if(found_numbers != 6) {
        cout << "\n" << "Invalid number of field checked. Please check only 6" << "\n\n";
    } else {
        cout << "\n\n";
    }
}
void LottoProcessor::drawSquares( Mat& image, const vector<vector<Point> >& squares )
{
    for( size_t i = 0; i < squares.size(); i++ )
    {
        const Point* p = &squares[i][0];
        int n = (int)squares[i].size();
        int avg = 0;
        int sum = 0;

        for(int x = squares[i][0].x; x < squares[i][2].x; x++) {
            for(int y = squares[i][2].y; y < squares[i][0].y; y++) {
                Scalar intensity = image.at<uchar>(Point(x, y));
                avg += intensity.val[0];
                sum++;
            }
        }
        avg = round(avg / sum);
        if(avg < 140) {
            polylines(image, &p, &n, 1, true, Scalar(0,255,0), 5, LINE_AA);
        }
    }
    imshow("Numbers", image);
}
vector<vector<Point>> LottoProcessor::getPattern(int field)
{
    int x, y, fpx = 103, fpy = 47;
    switch(field) {
        case 1:
            fpx = 267;
            fpy = 49;
        break;
        case 2:
            fpx = 431;
            fpy = 51;
        break;
        case 3:
            fpx = 595;
            fpy = 52;
        break;
        case 4:
            fpx = 102;
            fpy = 211;
        break;
        case 5:
            fpx = 266;
            fpy = 213;
        break;
        case 6:
            fpx = 429;
            fpy = 214;
        break;
        case 7:
            fpx = 592;
            fpy = 215;
        break;
        default:
            fpx = 103;
            fpy = 47;
        break;
    }
    
	vector<vector<Point>> squares;
    vector<vector<Point>>::iterator squares_iterator;

    squares_iterator = squares.begin();

    for(y = 0; y < 7; y++) {
        for(x = 0; x < 7; x++) {
            vector<Point> square;
            vector<Point>::iterator square_iterator;
            square_iterator = square.begin();

            Point tl = Point(fpx + (20 * x), fpy + ( 20 * y));
            Point tr = Point(tl.x + 15, tl.y);
            Point br = Point(tl.x + 15, tl.y + 15);
            Point bl = Point(tl.x, tl.y + 15);

            square_iterator = square.insert(square_iterator, tl);
            square_iterator = square.insert(square_iterator, tr);
            square_iterator = square.insert(square_iterator, br);
            square_iterator = square.insert(square_iterator, bl);

            squares_iterator = squares.insert(squares_iterator, square);

            if(y == 6 && x == 2) {
                break;
            }
        }
    }

    return squares;
}